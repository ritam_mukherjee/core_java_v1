package concepts.threads.producer_consumer.pluralsight_programs.producer_consumer_lock_problem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Source       :   PluralSight
 */
public class Thread_ProducerConsumer_Lock_Problem {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_CYAN = "\u001B[36m";
    static int count = 0;

    static boolean isEmpty(List<Integer> buffer) {
        return count == 0;
    }

    static boolean isFull(List<Integer> buffer) {
        return count == buffer.size();
    }

    public static void main(String[] args) throws InterruptedException {
        Lock lock=new ReentrantLock();

        Condition isEmpty=lock.newCondition();
        Condition isFull=lock.newCondition();

        List<Integer> buffer=new ArrayList<Integer>();


        class Consumer implements Callable<String> {

            @Override
            public String call() {


                while (count++ <= 50) {
                    try {
                        lock.lock();
                        while (isEmpty(buffer)) {
                            //wait
                            isEmpty.await();
                        }
                        buffer.remove(buffer.size() - 1);
                        //signal
                        isFull.signalAll();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        lock.unlock();
                    }
                }
                return ANSI_GREEN+" Consumed :" + (count - 1);
            }
        }
        class Producer implements Callable<String>{


            @Override
            public String call() throws Exception {
                int count=0;

                while (count++<=50) {
                    try {
                        lock.lock();
                        while(isFull(buffer)){
                            //wait
                            isFull.await();
                        }
                        buffer.add(1);
                        isEmpty.signalAll();
                        //signal
                    }finally {
                        lock.unlock();
                    }
                }
                return ANSI_RED+"produced "+(count-1);
            }
        }


        List<Producer> producers=new ArrayList<>();
        for (int i = 0; i <4 ; i++) {
            producers.add(new Producer());
        }

        List<Consumer> consumers=new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            consumers.add(new Consumer());
        }

        System.out.println("producer and consumers added");



        List<Callable<String>> producers_consumers=new ArrayList<>();
        producers_consumers.addAll(producers);
        producers_consumers.addAll(consumers);
        ExecutorService executorService= Executors.newFixedThreadPool(8);
        try {
            List<Future<String>> futures=executorService.invokeAll(producers_consumers);

            futures.forEach(
                    stringFuture -> {
                        try {
                            System.out.println(stringFuture.get());
                        }catch (InterruptedException ie){
                            ie.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }

                    }
            );
        }catch(Exception e){

        }
    /*    buffer = new int[10];
        count = 0;

        Producer producer = new Producer();
        Consumer consumer = new Consumer();
        Runnable produceTask = () -> {
            for (int i = 0; i < 50; i++) {
                producer.produce();
            }
            System.out.println(ANSI_GREEN + " Producing Done");
        };

        Runnable consumeTask = () -> {
            for (int i = 0; i < 50; i++) {
                consumer.consume();
            }
            System.out.println(ANSI_RED + " Consuming Done");
        };

        Thread producer_thread = new Thread(produceTask);
        Thread consumer_thread = new Thread(consumeTask);

        consumer_thread.start();
        producer_thread.start();

        consumer_thread.join();
        producer_thread.join();

        System.out.println(ANSI_CYAN + "Data in the buffer is   :" + count);*/

/*
        HERE WE WILL GET DESIRED OUTPUT
*/
    }
}
