package concepts.loops;

import java.util.Arrays;
import java.util.function.Consumer;

/**
 * Ref  :   https://www.w3resource.com/java-exercises/array/java-array-exercise-10.php
 * purpose  :   find maximum and minimum element of array in single rotation
 */
public class FindMaxMinElemets {

   static Consumer<int[]> calculate_max_min=ints -> {
        System.out.println(" Original Array: "+Arrays.toString(ints));
        int max=ints[0];
        int min=ints[0];
        for (int i = 0; i < ints.length; i++) {
            if(ints[i]>max) max=ints[i];
            if(ints[i]<min) min=ints[i];
        }
        System.out.println(" Maximum value for the above array = " + max);
        System.out.println(" Minimum value for the above array = " + min);
    };
    public static void main(String[] args) {

        int[] my_array = {25, 14, 56, 15, 36, 56, 77, 18, 29, 49};
        calculate_max_min.accept(my_array);


    }

}
