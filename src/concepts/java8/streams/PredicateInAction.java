package concepts.java8.streams;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static concepts.java8.streams.ColorConstants.*;

public class PredicateInAction {
    public static void main(String[] args) {


        Stream<String> numberStream=Stream.of("one","two","three","four","five","six","seven","eight","nine","zero");

        Predicate<String> isSizeMoreThanThree=s -> s.length()>3;
        Predicate<String> isEqualTwo=Predicate.isEqual("two");
        Predicate<String> isEqualThree=Predicate.isEqual("three");

        numberStream.filter(isEqualTwo.or(isEqualThree)).forEach(System.out::println);

        //System.out.println(ANSI_RED+"is all numbers size more than three?"+ANSI_RESET+numberStream.allMatch(isSizeMoreThanThree));



     //   System.out.println(ANSI_RED+"is any numbers size more than three?"+ANSI_RESET+numberStream.distinct().anyMatch(isSizeMoreThanThree));

    }
}
