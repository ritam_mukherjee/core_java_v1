------------------------------------------------------------------
package structure   :   java.util.stream
short Signature     :   Interface Stream<T>
                        Type Parameters:
                        T - the type of the stream elements
All Superinterfaces :
                    AutoCloseable, BaseStream<T,Stream<T>>


Full Signature      :
                    public interface Stream<T>
                                                                       extends BaseStream<T,Stream<T>>

 Definition         :
  A sequence of elements supporting sequential and parallel aggregate operations.
  It is an object on which we can define operation.


 Example            :The following example illustrates an aggregate operation using Stream and IntStream:
     int sum = widgets.stream()
                      .filter(w -> w.getColor() == RED)
                      .mapToInt(w -> w.getWeight())
                      .sum();


Reference           : https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html

Properties          :
1. Stream its not at all collection but not at all even though looks like.
2. It provides a way to efficiently process large amount of data ..and also smaller once.
3. It is an object on which we can define operation.
4. But the Stream object does not hold any data.
5. It also does not change any data that process
    [ reason is data is parallel distributed, by any change can cause visibility issue]
6. Able o process data in one pass.



QUESTION 1: Why the processing is efficient?
1. It leverage the computing power of multi-core CPUs by processing the data parallel
    Palatalization of the algorithm does not written by the developer.
2. All the processes conducted in a pipeline to avoid unnecessary intermediary computation.

QUESTION 2: Why can't we make collection as stream?
The reason is it was not intended to change the way collection API works and make backward compatibility.


LAZY Operation in Stream
-------------------------
The call of any filtered method is Lazy.
All the methods in a stream that returns another stream are lazy.
An operation on a stream that returns a stream is called intermediary operation.


there are two method we can apply over a stream one is peak() and another one is forEach() but there are some difference:
foreach() : does not return anything sometime seems as intermediary but work as final operation.
peek(): it is a intermediary operation return a stream.

The stream API defines fer intermediary operations:
	forEach(Consumer)  	[ not lazy]		does not return anything sometime seems as intermediary but work as final operation.
	peek(Consumer)		[ lazy]			it is a intermediary operation return a stream.
	filter(Consumer)	[ lazy]
	map(Consumer)		[ lazy]
	flatmap(Consumer)		[ lazy]			returns a stream so it is an intermediary operation.
	
Map Operation
--------------

