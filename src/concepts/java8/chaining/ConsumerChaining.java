package concepts.java8.chaining;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class ConsumerChaining {
    public static void main(String[] args) {
        List<Person> personList=new ArrayList<>();

        Consumer<Person> addConsumer=personList::add;
        Consumer<Person> printConsumer=System.out::println;

        Consumer<Person> addPrint=addConsumer.andThen(printConsumer);

        addPrint.accept(new Person("Sam", "Sharma", 22));
        addPrint.accept(new Person("Ravi", "Kumar", 25));
        addPrint.accept(new Person("Gopal", "Thakur", 28));

        System.out.println("-------------------------------------");
        Function<Person,String> convertFunction= person -> "Hello Mr."+person.getFirstname()+" "+person.getLastname();
        //Function l=convertFunction.apply(p)
        Consumer<String> printChangeList=System.out::println;

       /* peek is an intermediary method which not do any changes*/
        Consumer<List<Person>> changeList1=(personList1)->personList1.stream()
                .peek(convertFunction::apply).forEach(System.out::println);

        changeList1.accept(personList);

        System.out.println("-------------------------------------");

        /* map is not an intermediary method which not do any changes*/

        Consumer<List<Person>> changeList2=(personList1)->personList1.stream()
                .map(convertFunction::apply).forEach(System.out::println);

            changeList2.accept(personList);

    }
}
