package design_patterns.structural.decorator.ram_n.shape;
interface Shape {
    public void draw();
}
class Circle implements Shape{
    @Override
    public void draw() {
        System.out.println("shape:circle has drawn");
    }
}


class Triangle implements Shape{
    @Override
    public void draw() {
        System.out.println("shape:triangle has drawn");
    }
}

abstract class ShapeDecorator implements Shape{
    protected Shape decoratorShape;

    public ShapeDecorator(Shape decoratorShape) {
        this.decoratorShape = decoratorShape;
    }

    @Override
    public void draw() {
        decoratorShape.draw();
    }
}

class BlueShapeDecorator extends ShapeDecorator{


    public BlueShapeDecorator(Shape decoratorShape) {
        super(decoratorShape);
    }

    @Override
    public void draw() {
        decoratorShape.draw();
        setColor(decoratorShape);
    }

    private void setColor(Shape decoratorShape){
        System.out.println("color blue has applied to decorator shape:"+decoratorShape);
    }
}
public class ShapeDecoratorImplementation {

    public static void main(String[] args) {
        Shape blueCircle=new BlueShapeDecorator(new Circle());

        Shape blueTriangle=new BlueShapeDecorator(new Triangle());

        blueCircle.draw();
        blueTriangle.draw();
    }
}
