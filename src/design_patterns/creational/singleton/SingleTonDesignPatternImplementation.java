package design_patterns.creational.singleton;

import java.io.Serializable;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;

class Singleton implements Cloneable,Serializable{
    private  static  Singleton singleton;

    private Singleton() {
    }

    public static synchronized Singleton getInstance(){
        if(singleton.getInstance()==null){
            singleton=new Singleton();
        }

        return singleton;
    }

    private static Singleton getSingleton() {
        return singleton;
    }

    private static void setSingleton(Singleton singleton) {
        Singleton.singleton = singleton;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Singleton{},Hashcode    :"+this.hashCode();
    }
}
public class SingleTonDesignPatternImplementation {



    public static void main(String[] args) throws Exception {

       Singleton s=Singleton.getInstance();
        System.out.println(s.hashCode());
    }
}
